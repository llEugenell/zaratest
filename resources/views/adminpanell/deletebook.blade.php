<div id="deletebook" class="blck">
    <table border="1" class="deletebooktable">
        <tr>
            <th>id</th>
            <th>Name</th>
            <th>Author</th>
            <th>Genre</th>
            <th>Delete</th>
        </tr>
        @foreach($books as $book)
            <tr data-id="book{{$book->id}}">
                <td>{{$book->id}}</td>
                <td>{{$book->name}}</td>
                <td>{{$book->authors->name}}  {{$book->authors->lastname}}  {{$book->authors->surname}}</td>
                <td>{{$book->genre->name}}</td>
                <td><button type="submit" class="deletebookbutt" id="{{$book->id}}">Delete</button></td>
            </tr>
        @endforeach
    </table>


</div>