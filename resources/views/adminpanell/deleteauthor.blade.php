<div id="deleteauthor" class="blck">
    <table border="1" class="deleteauthortable">
        <tr>
            <th>id</th>
            <th>name</th>
            <th>surname</th>
            <th>lastname</th>
            <th>Delete</th>
        </tr>
        @foreach($authors as $author)
            <tr data-id="author{{$author->id}}">
                <td>{{$author->id}}</td>
                <td>{{$author->name}}</td>
                <td>{{$author->lastname}}  </td>
                <td>{{$author->surname}}</td>
                <td><button type="submit" id="{{$author->id}}" class="deleteauthorbutt">Delete</button></td>
            </tr>
        @endforeach
    </table>

</div>