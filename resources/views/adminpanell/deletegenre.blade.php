<div id="deletegenre" class="blck">
    <table border="1" class="deletegenretable">
        <tr>
            <th>id</th>
            <th>name</th>
            <th>Delete</th>
        </tr>
        @foreach($genre as $item)
            <tr data-id="genre{{$item->id}}">
                <td>{{$item->id}}</td>
                <td>{{$item->name}}</td>
                <td><button type="submit" class="deletegenrebutt" id="{{$item->id}}">Delete</button></td>
            </tr>
        @endforeach
    </table>

</div>