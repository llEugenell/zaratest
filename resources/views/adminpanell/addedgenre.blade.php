
<div class="addedgenre">
    <h2>Added genre</h2>
    {!! Form::open(array('url' => '/admin/addedgenre','method'=>'POST')) !!}
    <div class="miniblock">
        {!! Form::label('genre','Genre') !!}
        {!! Form::text('name') !!}
    </div>
    <div class="miniblock">
        {{Form::hidden('attribute','genre')}}

        {{Form::submit('Added Genre')}}
        {!! Form::close() !!}
    </div>
</div>
