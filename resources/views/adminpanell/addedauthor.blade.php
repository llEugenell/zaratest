
<div class="addedauthor">
    <h2>Added author</h2>
    {!! Form::open(array('url' => '/admin/addedauthor','method'=>'POST')) !!}
    <div class="miniblock">
         {!! Form::label('name','Name') !!}
            {!! Form::text('name') !!}

    </div>
    <div class="miniblock">
         {!! Form::label('lastname','Lastname') !!}
            {!! Form::text('lastname') !!}

    </div>
    <div class="miniblock">
        {!! Form::label('surname','Surname') !!}
        {!! Form::text('surname') !!}

    </div>

    {{Form::hidden('attribute','author')}}

    {{Form::submit('Added author')}}
    {!! Form::close() !!}

    <p class="requestadedauthor"></p>
</div>
