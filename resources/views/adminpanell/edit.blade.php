<div id="editbook" class="blck">
<table border="1">
    <caption>Edit</caption>
    <tr>
        <th>id</th>
        <th>name book</th>
        <th>name</th>
        <th>surname</th>
        <th>lastname</th>
        <th>genre</th>
        <th>edit</th>
    </tr>
    @foreach($books as $book)
        <tr>
            <td style="width:30px;">{{$book->id}}</td>
            <td>{{$book->name}}</td>
            <td>{{$book->authors->name}}    </td>
            <td>{{$book->authors->lastname}}</td>
            <td>{{$book->authors->surname}}</td>
            <td>{{$book->genre->name}}</td>
            <td><button type="button" class="editbutt">Edit</button></td>
        </tr>
    @endforeach
    <tr>
        {!! Form::open(array('url' => '/admin/edit','method'=>'POST')) !!}
        <td>  {!! Form::text('id',null,['class' => 'id', 'readonly' => 'true']) !!}</td>
        <td>{!! Form::text('book','',['class'=>'book']) !!}</td>
        <td>{!! Form::text('name_author','',['class'=>'name_author']) !!}</td>
        <td>{!! Form::text('surname_author','',['class'=>'surname_author']) !!}</td>
        <td>{!! Form::text('lastname_author','',['class'=>'lastname_author']) !!}</td>
        <td>{!! Form::text('genre','',['class'=>'genre']) !!}</td>
        <td> {{Form::submit('Save')}}</td>
        {!! Form::close() !!}
    </tr>
</table>


</div>