
    <div class="addedbook">
        <h2>Added book</h2>
        <div class="miniblock">
            {!! Form::open(array('url' => '/admin/addedbook','method'=>'POST')) !!}
            {!! Form::label('book_name','Name') !!}
            {!! Form::text('book') !!}
            {{Form::hidden('attribute','book')}}

        </div>
        <div class="miniblock">
            <p>Choice Author  <select class="author" name="author_id">

                    @foreach($authors as $author)
                        <option value="{{$author->id}}">{{$author->name}}  {{$author->surname}}  {{$author->lastname}}
                    @endforeach
                </select></p>

        </div>
        <div class="miniblock">
            {!! Form::select('genre_id', $genre->pluck('name', 'id')) !!}

        </div>
        {{Form::submit('Added Book')}}
        {!! Form::close() !!}


    </div>
