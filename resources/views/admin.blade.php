<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin Panell</title>


    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{asset('js/script.js')}}"></script>

</head>
    <body>

    @if(Session::get('enter')!='ok')
        <div class="enter">
        {!! Form::open(array('url' => '/admin/enter','method'=>'POST')) !!}
        {!! Form::label('name','Name') !!}
        {!! Form::text('name') !!}
        {!! Form::label('pass','Password') !!}
        {!! Form::text('password') !!}
        {{Form::submit('Enter')}}
        {!! Form::close() !!}
            @if(isset($enter))
            <p style="color:red;">{{$enter}}</p>
                @endif
        </div>
    @else
    <div class="header">
        @if(isset($msg))
           <p style="color:red;">{{$msg}}</p>
        @endif
        <p><img src="{{ asset('images/icon.jpg') }}" alt=""/></p>
       <p> Admin panell by Eugene©</p>
    </div>

    <div class="leftside">
        <ul class="menu"><p >Menu</p>
            <li><a href="#" class="addedall">Add</a></li>
            <ul class="addedmenu">
                <li><a href="{{$url}}/admin/addedbook" class="addedbookmenu">book</a></li>
                <li><a href="{{$url}}/admin/addedauthor" class="addedauthormenu">author</a></li>
                <li><a href="{{$url}}/admin/addedgenre" class="addedgenremenu">genre</a></li>
            </ul>
            <li><a href="{{$url}}/admin/edit" class="editall">Edit</a></li>
            <li><a href="#" class="submenudelete">Delete</a>
            <ul class="deletemenu">
                <li><a href="{{$url}}/admin/deletebook" class="delbook">book</a></li>
                <li><a href="{{$url}}/admin/deleteauthor" class="delauthor">author</a></li>
                <li><a href="{{$url}}/admin/deletegenre" class="delgenre">genre</a></li>
            </ul>
            </li>
        </ul>
    </div>

    <div class="rightside">
        @if(isset($incl))
            @include($incl)
        @endif


    </div>


        @endif
    </body>
</html>