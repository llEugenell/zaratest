<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
                {!! Form::open(array('url' => '/','method'=>'POST')) !!}
                {{Form::hidden('attribute','reset')}}
                {{Form::submit('Reset')}}
                {!! Form::close() !!}
            <div class="table" >
                <table border="1">
                    <caption>Книги</caption>
                    <tr>
                        <th>id</th>
                        <th>Имя</th>
                        <th>Автор</th>
                        <th>Жанр</th>
                    </tr>
        @foreach($books as $book)
            <tr>
                <td>{{$book->id}}</td>
                <td>{{$book->name}}</td>
                <td>{{$book->authors->name}}  {{$book->authors->lastname}}  {{$book->authors->surname}}</td>
                <td>{{$book->genre->name}}</td>
            </tr>
            @endforeach
                </table>
            </div>
                <div class="search" style="bottom: 0px;">
                    {!! Form::open(array('url' => '/','method'=>'POST')) !!}
                    {!! Form::label('book_name','Name') !!}
                    {!! Form::text('book_name') !!}
                    {{Form::hidden('attribute','book')}}
                    {{Form::submit('Search books')}}
                    {!! Form::close() !!}

                    {!! Form::open(array('url' => '/','method'=>'POST')) !!}
                    {!! Form::label('author','Author name') !!}
                    {!! Form::text('author') !!}
                    {{Form::hidden('attribute','author')}}
                    {{Form::submit('Search author')}}
                    {!! Form::close() !!}

                    {!! Form::open(array('url' => '/','method'=>'POST')) !!}
                    {!! Form::label('genre','Genre') !!}
                    {!! Form::text('genre') !!}
                    {{Form::hidden('attribute','genre')}}
                    {{Form::submit('Search genre')}}
                    {!! Form::close() !!}
                </div>

        </div>

    </body>
</html>
