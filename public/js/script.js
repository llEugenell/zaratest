$( document ).ready(function() {
    $('.addedall').on("click", function () {
        if ($('.addedmenu').css('display') == 'none') {
            $('.addedmenu').css('display', 'block');
        } else {
            $('.addedmenu').css('display', 'none');
        }
    });
    let k = 0;

    $('.submenudelete').on("click", function () {
        if (k == 0) {
            $('.deletemenu').css('display', 'block');
            k = 1;
        } else {
            $('.deletemenu').css('display', 'none');
            k = 0;
        }
    });


    $('.deleteauthorbutt').on("click", function () {
        $.ajax({
            type: 'POST',
            url: '/admin',
            data: {id: $(this).attr('id'), attribute: 'author'},
            headers: {

                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')

            },
            success: function (data) {
                let id = 'author' + data;
                $('[data-id~=' + id + ']').closest("tr").remove();
            },
            error: function () {
            }
        });

    });

    $('.deletebookbutt').on("click", function () {
        $.ajax({
            type: 'POST',
            url: '/admin',
            data: {id: $(this).attr('id'), attribute: 'book'},
            headers: {

                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')

            },
            success: function (data) {
                let id = 'book' + data;
                $('[data-id~=' + id + ']').closest("tr").remove();
            },
            error: function () {
                console.log('123');
            }
        });
    });

    $('.deletegenrebutt').on("click", function () {
        $.ajax({
            type: 'POST',
            url: '/admin',
            data: {id: $(this).attr('id'), attribute: 'genre'},
            headers: {

                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')

            },
            success: function (data) {
                let id = 'genre' + data;
                $('[data-id~=' + id + ']').closest("tr").remove();
            },
            error: function () {
                console.log('123');
            }
        });
    });

    $('.editbutt').on("click", function () {
        $('.genre').val($(this).parent().prev().text());
        $('.lastname_author').val($(this).parent().prev().prev().text());
        $('.surname_author').val($(this).parent().prev().prev().prev().text());
        $('.name_author').val($(this).parent().prev().prev().prev().prev().text());
        $('.book').val($(this).parent().prev().prev().prev().prev().prev().text());
        $('.id').val($(this).parent().prev().prev().prev().prev().prev().prev().text());
        $("html, body").animate({scrollTop: $("header").height() + 7200}, "slow");
    });
});