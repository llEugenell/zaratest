<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','BooksController@show');
Route::get('/test','BooksController@test');
Route::post('/','BooksController@find');
Route::post('/admin/addedbook','AdminController@add');
Route::post('/admin/addedauthor','AdminController@add');
Route::post('/admin/addedgenre','AdminController@add');
Route::post('/admin/edit','AdminController@edit');
Route::get('/admin/{id}','AdminController@menunavigation');
Route::get('/admin','AdminController@show');
Route::post('/admin','AdminController@delete');
Route::post('//admin/enter','AdminController@adminLogin');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
