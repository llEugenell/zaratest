<?php namespace Pawlox\RabbitMQ;

use Config;
use Pawlox\RabbitMQ\Connection;
use Pawlox\RabbitMQ\BaseOptions;
use Illuminate\Config\Repository;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Connection\AMQPConnection;

/**
 * Message class, used to manage messages back and forth with the server
 *
 */
class Message extends BaseOptions {


    /**
     * Message to be send or received from the queue server
     *
     * @var string
     */
    public $message;

    /**
     * Message constructor
     *
     * @param array $options  Options array to get validated
     *
     * @return Pawlox\RabbitMQ\Message
     */
    public function __construct(Repository $config, array $options = NULL)
    {
        parent::__construct($config);

        if ($options)
            $this->setOptions($options);
    }

    /**
     * Add a message directly to the queue server
     *
     * @param string $routing_key  Routing key on RabbitMQ
     * @param string $message  Message to be add to the queue server
     * @param array $options  Options values for message to add
     *
     * @return void
     */
    public function add($routing_key, $message, array $options = NULL)
    {
        $this->routing_key = $routing_key;
        $this->message = $message;

        if ($options)
            $this->setOptions($options);

        $this->save();
    }

    /**
     * Save the current message instance into de queue server
     *
     * @return void
     */
    public function save()
    {
        try
        {
            $connection = new Connection($this->buildConnectionOptions());
            $connection->open();

            $properties_array = [
                'content_type' => $this->content_type,
                'delivery_mode' => 2
            ];

            if ($this->expiration) {
                $properties_array['expiration'] = $this->expiration;
            }

            $msg = new AMQPMessage($this->message, $properties_array);
            $connection->channel->basic_publish($msg, $this->exchange, $this->routing_key);

            $connection->close();
        }
        catch (Exception $e)
        {
            $connection->close();
            throw new Exception($e);
        }
    }

}