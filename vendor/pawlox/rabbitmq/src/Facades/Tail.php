<?php namespace Pawlox\RabbitMQ\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * RabbitMQ Facade Class
 *
 */
class RabbitMQ extends Facade {
    
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'RabbitMQ'; }

}