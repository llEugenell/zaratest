<?php namespace Pawlox\RabbitMQ;

/**
 * Lumen Service Provider
 *
 */
class LumenServiceProvider extends \Illuminate\Support\ServiceProvider {

   
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //Bind config
        $this->app->bind('Pawlox\RabbitMQ\Message', function ($app) {
            return new Message($app->config);
        });
        $this->app->bind('Pawlox\RabbitMQ\Listener', function ($app) {
            return new Listener($app->config);
        });

        //Register Facade
        $this->app->bind('RabbitMQ', 'Pawlox\RabbitMQ\RabbitMQ');

        if (!class_exists('RabbitMQ')) {
            class_alias('Pawlox\RabbitMQ\Facades\RabbitMQ', 'RabbitMQ');
        }

        //Add App facade if is lumen >5.2
        if ($this->version() >= 5.2) {
            class_alias('Illuminate\Support\Facades\App', 'App');
        }       
    }

    /** 
     * Get Lumen version
     */
    protected function version()
    {
        $version = explode('(', $this->app->version());
        if (isset($version[1])) {
            return substr($version[1], 0, 3);
        }        
        return null;
    }

}