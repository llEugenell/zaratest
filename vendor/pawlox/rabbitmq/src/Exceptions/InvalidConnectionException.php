<?php namespace Pawlox\RabbitMQ\Exceptions;

use Exception;

/**
 * Invalid Connection Exception
 *
 */
class InvalidConnectionException extends Exception {
    
}