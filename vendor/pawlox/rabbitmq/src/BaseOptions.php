<?php namespace Pawlox\RabbitMQ;

use Config;
use Illuminate\Config\Repository;
use Pawlox\RabbitMQ\Exceptions\InvalidOptionException;

/**
 * Base class options used to wrap common methods for connection, listening and adding messages
 *
 */
class BaseOptions {

    /**
     * Valid options array, include all valid options can be set
     *
     * @var array
     */
    protected $allowedOptions = [
        'exchange',
        'exchange_type',
        'vhost',
        'connection_name',
        'queue_name',
        'content_type',
        'routing_key',
        'create_queue',
        'expiration'
    ];

    /**
     * Config repository dependency
     *
     * @var Illuminate\Config\Repository
     */
    protected $config;

    /**
     * Exchange name on RabbitMQ Server
     *
     * @var string
     */
    public $exchange;

    /**
     * Virtual host name on RabbitMQ Server
     *
     * @var string
     */
    public $vhost;

    /**
     * Connection name defined in config/tail-settings.php file
     *
     * @var string
     */
    public $connection_name;

    /**
     * Queue name for this connection
     *
     * @var string
     */
    public $queue_name;

    /**
     * Routing key for connection
     *
     * @var string
     */
    public $routing_key;

    /**
     * RabbitMQ AMQP exchange type
     * should be one of:
     *      direct, fanout, topic or headers
     *
     * @var string
     */
    public $exchange_type;

    /**
     * Content-Type for the messages send over this connection
     *
     * @var string
     */
    public $content_type;

    /**
     * Is this connection should try to declare queue?
     *
     * @var boolean
     */
    public $create_queue;

    /**
     * Message TTL time in milliseconds
     *
     * @var boolean
     */
    public $expiration;

    /**
     * Constructor
     *
     * @param Illuminate\Config\Repository $config  Config dependency
     *
     * @return Pawlox\RabbitMQ\BaseOptions
     */
    public function __construct(Repository $config)
    {
        $this->config = $config;
    }

    /**
     * Validate the given options with the allowed options
     *
     * @param array $options  Options array to get validated
     *
     * @return Pawlox\RabbitMQ\BaseOptions
     */
    public function validateOptions(array $options)
    {
        foreach ($options as $option => $value)
        {
            if (!in_array($option, $this->allowedOptions))
                throw new InvalidOptionException("Option [$option] is not valid");
        }

        return $this;
    }

    /**
     * Set the following options in the class
     *
     * @param array $options  Options with values to be set
     *
     * @return Pawlox\RabbitMQ\BaseOptions
     */
    public function setOptions(array $options)
    {
        //Validate options
        $this->validateOptions($options);

        //Set options
        foreach ($options as $option => $value)
            $this->$option = $value;

        return $this;
    }

    /**
     * Build options set to build a connection to the queue server
     *
     * @return array
     */
    public function buildConnectionOptions()
    {
        //Default connection
        $connection_name = $this->config->get("rabbitmq-settings.default");

        //Check if set for this connection
        if ($this->connection_name)
            $connection_name = $this->connection_name;

        $connectionOptions = $this->config->get("rabbitmq-settings.connections.$connection_name");

        //Set current instance properties values
        if ($this->vhost)
            $connectionOptions['vhost'] = $this->vhost;
        if ($this->exchange)
            $connectionOptions['exchange'] = $this->exchange;
        if ($this->routing_key)
            $connectionOptions['routing_key'] = $this->routing_key;

        //Queue specific options
        $connectionOptions['queue_name'] = $this->queue_name;

        //Declare queue?
        if ($this->create_queue) {
            $connectionOptions['create_queue'] = $this->create_queue;
        }

        return $connectionOptions;
    }
}