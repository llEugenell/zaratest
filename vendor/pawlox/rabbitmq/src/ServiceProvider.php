<?php namespace Pawlox\RabbitMQ;

/**
 * Service Provider
 *
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider {

   
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/rabbitmq.php' => config_path('rabbitmq-settings.php'),
        ], 'config');
        
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/rabbitmq.php', 'rabbitmq-settings'
        );
        
        //Register Facade
        $this->app->bind('RabbitMQ', 'Pawlox\RabbitMQ\RabbitMQ');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}