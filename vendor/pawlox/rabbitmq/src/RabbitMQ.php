<?php namespace Pawlox\RabbitMQ;

use App;
use Closure;
use Pawlox\RabbitMQ\Message;
use Pawlox\RabbitMQ\Listener;

/**
 * RabbitMQ class, used as facade handler
 *
 */
class RabbitMQ {

    /**
     * Add a message directly to the queue server
     *
     * @param string $routing_key  Routing key on RabbitMQ
     * @param string $message  Message to be add to the queue server
     * @param array $options  Options values for message to add
     *
     * @return void
     */
    public function add($routing_key, $message, array $options = null)
    {
        $msg = App::make('Pawlox\RabbitMQ\Message');
        $msg->add($routing_key, $message, $options);
    }

    /**
     * Create new blank message instance
     *
     * @return Pawlox\RabbitMQ\Message
     */
    public function createMessage()
    {
        return App::make('Pawlox\RabbitMQ\Message');
    }

    /**
     * Listen queue server for given queue name
     *
     * @param string $queue_name  Queue name to listen
     * @param array $options  Options to listen
     *
     * @return void
     */
    public function listen($queue_name, Closure $callback)
    {
        $listener = App::make('Pawlox\RabbitMQ\Listener');
        $listener->listen($queue_name, null, $callback);
    }

    /**
     * Listen queue server for given queue name
     *
     * @param string $queue_name  Queue name to listen
     * @param array $options  Options to listen
     * @param Closure $closure Function to run for every message
     *
     * @return void
     */
    public function listenWithOptions($queue_name, array $options, Closure $callback)
    {
        $listener = App::make('Pawlox\RabbitMQ\Listener');
        $listener->listen($queue_name, $options, $callback);
    }
    
}