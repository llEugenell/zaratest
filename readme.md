Описание проекта:
    Проект поиска книг которые имеются в бд , а также их добавление и рассылка пользователям на email

Для запуска необходимо:

1)Скопировать все файлы в рабочую директорию
2)Настроить env файл , а именно:
    APP_URL-указать ваш урл
    настроить подключение к бд-DB_CONNECTION,DB_HOST,DB_PORT,DB_DATABASE,DB_USERNAME,DB_PASSWORD
    настроить почту отправителя -MAIL_DRIVER=smtp,MAIL_HOST=smtp.gmail.com,MAIL_PORT=587,MAIL_USERNAME и MAIL_PASSWORD-
        ваше имя и пароль ,MAIL_ENCRYPTION=tls, так же разрешить вход в вашу почту для небезопасных приложений -https://myaccount.google.com/lesssecureapps
3)Дальше запускаем такие комманды из консоли:
    php artisan migrate
    php artisan db:seed
    php artisan queue:work --daemon --sleep=5
    (Также не забываем дать права на запись и чтение для папок /storage  и bootstrap/cache)
Поздравляю приложение готово к работе )))

Что бы получить рассылку новых книг не забываем зарегистрировать пользователя со своим email. Для входа в админку добавляем к URL '/admin',и
используем для входа данные-admin admin( имя и пароль соответственно)      

        
                                                                                       