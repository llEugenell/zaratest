<?php

use Illuminate\Database\Seeder;

class GenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Genre')->insert([
            'name' => 'Роман',
        ]);
        DB::table('Genre')->insert([
            'name' => 'Историческая драма',
        ]);
        DB::table('Genre')->insert([
            'name' => 'Поэма',
        ]);

    }
}
