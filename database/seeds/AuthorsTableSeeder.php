<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Authors')->insert([
            'name' => 'Федор',
            'lastname' => 'Михайлович',
            'surname' => 'Достоевский',
        ]);
        DB::table('Authors')->insert([
            'name' => 'Александр',
            'lastname' => 'Сергеевич',
            'surname' => 'Пушкин',
        ]);
        DB::table('Authors')->insert([
            'name' => 'Михаил',
            'lastname' => 'Юрьевич',
            'surname' => 'Лермонтов',
        ]);

    }
}
