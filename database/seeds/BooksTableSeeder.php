<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Books')->insert([
            'name' => 'Идиот',
            'author_id' => '1',
            'genre_id' => '1',
        ]);

        DB::table('Books')->insert([
            'name' => 'Игрок',
            'author_id' => '1',
            'genre_id' => '1',
        ]);
        DB::table('Books')->insert([
            'name' => 'Капитанская дочка',
            'author_id' => '2',
            'genre_id' => '2',
        ]);
        DB::table('Books')->insert([
            'name' => 'Руслан и Людмила',
            'author_id' => '2',
            'genre_id' => '3',
        ]);
        DB::table('Books')->insert([
            'name' => 'Герой нашего времени',
            'author_id' => '3',
            'genre_id' => '1',
        ]);

    }
}
