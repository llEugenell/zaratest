<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Authors;

class AuthorsController extends Controller
{
    //
    public function add(Request $request)
    {
        if(!empty($request->name)&&!empty($request->lastname)&&!empty($request->surname))
        {
            $author=new Authors();
            $author->name=$request->name;
            $author->lastname=$request->lastname;
            $author->surname=$request->surname;
            $author->save();
            return "You added new record";
        }else{
            return "fill in all input ";
        }
    }

    public function delete($id)
    {

        if(Authors::where('id',$id)->first())
        {
            $result=Authors::where('id',$id)->delete();
            return "the record delete";
        }


    }

    public function getAuthorById($id)
    {
        $author=Authors::where('id',$id)->first();
        $fio=$author->name." ".$author->lastname." ".$author->surname;
        return $fio;
    }

    public function edit($id,$name,$lastname,$surname)
    {
        $author=Authors::find($id);
        $author->name=$name;
        $author->lastname=$lastname;
        $author->surname=$surname;
        $author->save();
        return "ok";
    }


}
