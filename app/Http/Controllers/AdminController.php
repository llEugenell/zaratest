<?php

namespace App\Http\Controllers;

use App\Evenets\AddBookEvent;
use Illuminate\Http\Request;
use App\Books;
use App\Genre;
use App\Authors;
use URL;
use Session;
use Illuminate\Support\Facades\Event;



class AdminController extends Controller
{
    //
    private $bookcontroller;
    private $authorscontroler;
    private $genrecontroller;
    private $allbooks;
    private $allgenre;
    private $allauthors;

    /**
     * Get a controllers to a variable
     *
     *
     */
    public function __construct()
    {
        $this->bookcontroller=new BooksController();
        $this->authorscontroler=new AuthorsController();
        $this->genrecontroller=new GenreController();
        $this->allbooks=Books::all();
        $this->allgenre=Genre::all();
        $this->allauthors=Authors::all();
    }




    /**
     * Show a mainview
     *
     * @return view
     */
    public function show()
    {
        return view('admin', ['books' => $this->allbooks,'genre'=>$this->allgenre,'authors'=>$this->allauthors,'url'=>URL::to('/')]);
    }





    /**
     * Navigation for menu
     *
     * @return view with params
     */
    public function menunavigation($id)
    {

        switch ($id)
        {
            case'addedbook':
                return view('admin', ['incl'=>'adminpanell.addedbook','authors'=>$this->allauthors,'genre'=>$this->allgenre,'url'=>URL::to('/')]);

            case'addedauthor':
                return view('admin', ['incl'=>'adminpanell.addedauthor','url'=>URL::to('/')]);

            case 'addedgenre':
                return view('admin', ['incl'=>'adminpanell.addedgenre','url'=>URL::to('/')]);

            case 'edit':
                return view('admin', ['incl'=>'adminpanell.edit','books'=>$this->allbooks,'authors'=>$this->allauthors,'genre'=>$this->allgenre,'url'=>URL::to('/')]);

            case 'deletebook':
                return view('admin', ['incl'=>'adminpanell.deletebook','books'=>$this->allbooks,'url'=>URL::to('/')]);

            case 'deleteauthor':
                return view('admin', ['incl'=>'adminpanell.deleteauthor','authors'=>$this->allauthors,'url'=>URL::to('/')]);

            case 'deletegenre':
                return view('admin', ['incl'=>'adminpanell.deletegenre','genre'=>$this->allgenre,'url'=>URL::to('/')]);

        }
    }



    public function delete(Request $request)
    {
        switch ($request->attribute)
        {
            case 'author':
                $result=$this->authorscontroler->delete($request->id);
                return $request->id;
                break;

            case 'book':
                $result=$this->bookcontroller->delete($request->id);
                return $request->id;
                break;
            case'genre':
                $result=$this->genrecontroller->delete($request->id);
                return $request->id;
                break;
        }
    }


    public function add(Request $request)
    {
        switch ($request->attribute)
        {
            case'book':
               $res=$this->bookcontroller->add($request);
               return $this->menunavigation('addedbook')->with('msg',$res);
               break;
            case'genre':
                $res=$this->genrecontroller->add($request);
                return $this->menunavigation('addedgenre')->with('msg',$res);
                break;
            case'author':
                $res=$this->authorscontroler->add($request);
                return $this->menunavigation('addedauthor')->with('msg',$res);
                break;
        }
    }


    public function edit(Request $request)
    {

            $id=$request->id;
            $author_id=$this->bookcontroller->getAuthorId($id);
            $genre_id=$this->bookcontroller->getGenreId($id);
            $resbook=$this->bookcontroller->edit($id,$request->book);
            $resauthor=$this->authorscontroler->edit($author_id,$request->name_author,$request->surname_author,$request->lastname_author);
            $resgenre=$this->genrecontroller->edit($genre_id,$request->genre);
            return $this->menunavigation('edit')->with('msg','you edit is successful');
    }






    /**
     * Login form admin
     *
     *
     */
    public function adminLogin(Request $request)
    {
        if(!empty($request->name)&&!empty($request->password))
        {
            if($request->name=='admin'&&$request->password=='admin')
            {
                session(['enter'=>'ok']);
                return redirect('admin');

            }
        }else{
            return $this->show()->with('enter',"pls write password and name");
        }
    }

}
