<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;

class GenreController extends Controller
{
    //
    public function add(Request $request)
    {
        if(!empty($request->name))
        {
          $genre=new Genre();
          $genre->name=$request->name;
          $genre->save();
            return "You added new record";
        }else{
            return "fill in all input ";
        }

    }

    public function getGenreById($id)
    {
        $genre=Genre::where('id',$id)->first();
        return $genre->name;
    }

    public function delete($id)
    {
        if(Genre::where('id',$id)->first())
        {
            $result=Genre::where('id',$id)->delete();
            return "the record delete";
        }
    }

    public function edit($id,$name)
    {
        $genre=Genre::find($id);
        $genre->name=$name;
        $genre->save();
        return "ok";
    }
}
