<?php

namespace App\Http\Controllers;

use App\Evenets\AddBookEvent;
use Illuminate\Http\Request;
use App\Books;
use Illuminate\Support\Facades\Event;
use Mail;
use App\Mail\BookAdd;



class BooksController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function show()
   {

       $books=Books::all();
       return view('welcome', ['books' => $books]);

   }

   public function find(Request $request)
   {
        switch ($request->attribute)
        {
            case 'author':
             $name=$request->author;
             $posts = Books::whereHas('authors', function ($query) use($name) {
                 $query->where('name', 'like','%'.$name.'%')
                     ->orWhere('lastname', 'like','%'.$name.'%')
                     ->orWhere('surname', 'like','%'.$name.'%');
             })->get();
                return view('welcome', ['books' => $posts]);
             break;

            case 'book':
                $books=Books::where('name','like','%'.$request->book_name.'%')->get();
                return view('welcome', ['books' => $books]);
                break;

            case 'genre':
                $name=$request->genre;
                $posts = Books::whereHas('genre', function ($query) use($name) {
                    $query->where('name', 'like','%'.$name.'%');
                })->get();
                return view('welcome', ['books' => $posts]);
                break;

            case 'reset':
                $books=Books::all();
                return view('welcome', ['books' => $books]);
                break;
        }
   }

   public function add(Request $request)
   {
        if(!empty($request->book)){
            $book=new Books();
            $book->name=$request->book;
            $book->author_id=$request->author_id;
            $book->genre_id=$request->genre_id;
            $book->save();
            Event::dispatch(new AddBookEvent($book));
           // Mail::to('eugenelboichenko@gmail.com')->send(new BookAdd('hui'));
            return "You added new record";
        }else{
            return "fill in all input ";
        }
   }

   public function test()
   {
       $book=new Books();
       $book->name='rewrew';
       $book->author_id='1';
       $book->genre_id='1';
       $book->save();
   }

   public function delete($id)
   {
       if(Books::where('id',$id)->first())
       {
           $result=Books::where('id',$id)->delete();
           return "the record delete";
       }
   }

   public function getAuthorId($id)
   {
       $book=Books::where('id',$id)->first();
       return $book->author_id;
   }

   public function getGenreId($id)
   {
       $book=Books::where('id',$id)->first();
       return $book->genre_id;
   }

   public function edit($id,$name)
   {
       $book=Books::find($id);
       $book->name=$name;
       $book->save();
       return "ok";
   }
}
