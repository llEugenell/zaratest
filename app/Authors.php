<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authors extends Model
{
    //
    protected $table = 'Authors';
    public $timestamps = false;

    public function books()
    {
        return $this->hasMany('App\Books','author_id','id');
    }
}
