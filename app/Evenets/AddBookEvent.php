<?php

namespace App\Evenets;

use App\Books;
use App\Authors;
use App\User;
use App\Http\Controllers\AuthorsController;
use App\Http\Controllers\GenreController;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddBookEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $article_name;
    public $author;
    public $genre;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Books $books)
    {
        //
        $authors=new AuthorsController();
        $genres=new GenreController();
        $genre=$genres->getGenreById($books->genre_id);
        $author=$authors->getAuthorById($books->author_id);
        $this->article_name=$books->name;
        $this->author=$author;
        $this->genre=$genre;

    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
