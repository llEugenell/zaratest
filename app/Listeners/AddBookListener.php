<?php

namespace App\Listeners;

use App\Evenets\AddBookEvent;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Jobs\SendingMail;


class AddBookListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddBookEvent  $event
     * @return void
     */
    public function handle(AddBookEvent $event)
    {
        //
        $user=new User;
        $message='Вышла новая книга-'.$event->article_name.' Автор которой '.$event->author.' А Жанр - '.$event->genre;
        dispatch(new SendingMail($user,$message));

    }
}
