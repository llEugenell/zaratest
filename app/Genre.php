<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    //
    protected $table = 'Genre';
    public $timestamps = false;

    public function books()
    {
        return $this->hasOne('App\Books','genre_id');
    }
}
