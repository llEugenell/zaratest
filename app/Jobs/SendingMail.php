<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use App\User;
use Mail;
use App\Mail\BookAdd;

class SendingMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;
    protected $user;
    protected $message;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user,$message)
    {
        //
        $this->user=$user;
        $this->message=$message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $emails=$this->user->pluck('email');
        foreach ($emails as $email)
        {
            Mail::to($email)->send(new BookAdd($this->message));
        }
    }
}

