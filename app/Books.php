<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    //
    protected $table = 'Books';
    public $timestamps = false;

    public function authors()
    {
        return $this->belongsTo('App\Authors','author_id','id');
    }

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }


}
